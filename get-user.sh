#!/usr/bin/env bash

TERM="$1"
PAGES="$2"
TOKEN="$3"

if [ $# -lt 1 ]; then
echo "Sem nome de utilizador fica difcil..."
exit 1
fi

if [ -f $TERM.IDS.txt ]; then
echo "Ficheiro final já existe, eliminando para evitar ids duplicadas"
rm $TERM.IDS.txt
fi

if [ -z "$PAGES" ]
then
pagecount=1
noresults=0
while [ "$noresults" -eq 0 ]; do
  echo "a registar os resultados do utilizador $TERM na pagina $pagecount";
  curl -s \
    "https://videos.sapo.pt/ajax/video?username=$TERM&token=$TOKEN&page=$pagecount" \
    -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36' \
    -H 'Accept: text/javascript,text/xml,application/xml,application/xhtml+xml,text/html,application/json;q=0.9,text/plain;q=0.8,video/x-mng,image/png,image/jpeg,image/gif;q=0.2,*/*;q=0.1' \
    -H 'Accept-Language: en-US' \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'X-Requested-With: XMLHttpRequest' \
    -H "Cookie: language=pt; sso_tld=POR; bsu-v3-api=1689792348549; sv_token=$TOKEN" -o "$TERM.$pagecount.json"

   if [ x`cat "$TERM.$pagecount.json" | jq '.items_in_response'` = x0 ]; then
    echo "resultados já estão nulos, acabamos por aqui maltinha!"
    rm "$TERM.$pagecount.json"
    noresults=1
    pagecount=$(( $pagecount - 1 ))
    else
    cat "$TERM.$pagecount.json" | jq '.data[].randname' | sed 's/"//g' >> "$TERM.IDS.txt"
    pagecount=$(( $pagecount + 1 ))
    fi
    done

echo "Queres eliminar os resultados gravados (o balde cheio de ficheiros json que tens na pasta)?"
echo "(sim/não)?"
read del

if [ $del == 'sim' ];
then
for pagecount in $(seq 1 $pagecount);
do
rm "$TERM.$pagecount.json"
done
fi

else

for PAGE in $(seq 1 $PAGES); do
  echo "a gravar os resultados do utilizador $TERM na pagina $PAGE";
  curl -s \
    "https://videos.sapo.pt/ajax/video?username=$TERM&token=$TOKEN&page=$PAGE" \
    -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36' \
    -H 'Accept: text/javascript,text/xml,application/xml,application/xhtml+xml,text/html,application/json;q=0.9,text/plain;q=0.8,video/x-mng,image/png,image/jpeg,image/gif;q=0.2,*/*;q=0.1' \
    -H 'Accept-Language: en-US' \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'X-Requested-With: XMLHttpRequest' \
    -H "Cookie: language=pt; sso_tld=POR; bsu-v3-api=1689792348549; sv_token=$TOKEN" -o "$TERM.$PAGE.json"
    cat "$TERM.$PAGE.json" | jq '.data[].randname' | sed 's/"//g' >> "$TERM.IDS.txt"
done

echo "Queres eliminar os resultados gravados (a chuva de ficheiros json que tens na pasta)?"
echo "(sim/não)?"
read del

if [ $del == 'sim' ];
then
for PAGE in $(seq 1 $PAGES);
do
rm "$TERM.$PAGE.json"
done
fi

fi

echo 'Agora podes fazer: while read p; do yt-dlp "http://videos.sapo.pt/$p"; done < '$TERM.IDS.txt''
