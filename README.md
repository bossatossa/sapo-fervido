# Que raios é isto?
Por enquanto são 2 simples scripts de bash, um para obteres todos os ids de vídeos de um utilizador em particular (*get-user.sh*) e outro para obteres uma lista de utilizadores de uma categoria (*get-catagory-users.sh*).

Mas é suficiente para facilitar o resgate de sapos em panelas a ferver.

É baseado no trabalho já feito pelo [**Hugo Peixoto**](https://git.ansol.org/hugopeixoto/sapo-videos) e [**Ricardo Lafuente**](https://ciberlandia.pt/@rlafuente/110742965372804262).

## Pré-requisitos
1.  Teres uma instalação de bash e as utilidades básicas (praticamente garantido se tiveres no Linux ou Mac)
2. Verifica se tens o *curl* e *jq* instalados
3. Verifica tambem se tens o *yt-dlp* para conseguires usar a lista gerada pelo *get-user.sh*
