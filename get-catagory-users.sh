#!/usr/bin/env bash
TERM="$1"
TOKEN="8ae8ae963df60656362d6d542e888fb1"
pagecount=1
noresults=0
while [ "$noresults" -eq 0 ]; do
  echo "$TERM.$pagecount";
  curl -s \
    "https://videos.sapo.pt/ajax/category/$TERM?token=$TOKEN&nocache=9544&page=$pagecount&order=releve" \
    -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0' \
    -H 'Accept: text/javascript,text/xml,application/xml,application/xhtml+xml,text/html,application/json;q=0.9,text/plain;q=0.8,video/x-mng,image/png,image/jpeg,image/gif;q=0.2,*/*;q=0.1' \
    -H 'Accept-Language: en-US' \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'X-Requested-With: XMLHttpRequest' \
    -H "Cookie: language=pt; sso_tld=POR; bsu-v3-api=1689792348549; sv_token=$TOKEN" -o "category-$TERM.$pagecount.json"

   if [ x`cat "category-$TERM.$pagecount.json" | jq '.items_in_response'` = x0 ]; then
    echo "resultados já estão nulos, acabamos por aqui maltinha!"
    rm "category-$TERM.$pagecount.json"
    noresults=1
    pagecount=$(( $pagecount - 1 ))
    else
    cat "category-$TERM.$pagecount.json" | jq '.data[].author' | sed 's/"//g' | sort -f -b -u >> "$TERM.AUTHORS.txt"
    cat "$TERM.AUTHORS.txt" | sort -f -b -u -o "$TERM.AUTHORS.txt"
    pagecount=$(( $pagecount + 1 ))
    fi
    done

echo "Queres eliminar os resultados gravados (as varias centenas de ficheiros json que tens na pasta)?"
echo "(sim/não)?"
read del

if [ $del == 'sim' ];
then
for pagecount in $(seq 1 $pagecount);
do
rm "category-$TERM.$pagecount.json"
done
fi
